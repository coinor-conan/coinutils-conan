from pathlib import Path

from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.tools import SystemPackageTool


class CoinutilsConan(ConanFile):
    name = "coinutils"
    version = "2.11.4"
    license = "EPL-1.0"
    author = "Harald Held harald.held@gmail.com"
    url = "https://gitlab.com/coinor-conan/coinutils-conan"
    description = "CoinUtils (Coin-OR Utilities) is an open-source collection of classes and functions that are generally useful to more than one COIN-OR project."
    topics = ("OR", "optimization", "Coin-OR")
    settings = "os", "compiler", "build_type", "arch"
    generators = "pkg_config"

    def source(self):
        self.run("git clone -b releases/2.11.4 https://github.com/coin-or/CoinUtils.git .")

    def build(self):
        autotools = AutoToolsBuildEnvironment(self)
        autotools.configure()
        autotools.make()
        autotools.install()

    def system_requirements(self):
        installer = SystemPackageTool()
        installer.install("libblas-dev")
        installer.install("zlib1g-dev")
        installer.install("liblapack-dev")

    def package_info(self):
        with tools.environment_append(
                {'PKG_CONFIG_PATH': (Path(self.package_folder) / "lib" / "pkgconfig").as_posix()}):
            pkg_config = tools.PkgConfig("coinutils")

            # strip -l, -L
            self.cpp_info.libs = [lib[2:] for lib in pkg_config.libs_only_l]
            self.cpp_info.libdirs = [libdir[2:] for libdir in pkg_config.libs_only_L]

            self.cpp_info.cxxflags = pkg_config.cflags

            self.env_info.PKG_CONFIG_PATH.append((Path(self.package_folder) / "lib" / "pkgconfig").as_posix())
